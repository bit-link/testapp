<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Frontend Route
Route:: get('/', 'HomeController@index');





















// Backend Route
Route:: get('/admin', 'AdminController@index');
Route:: get ('/dashboard','AdminController@show_dashboard');
Route:: post ('/admin-dashboard','AdminController@dashboard');















// Route::get('/', function () {
//     return view('layout');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');


//  // Admin Authentication Routes...
//  $this->get('admin/home', 'AdminController@index');
//  $this->get('admin', 'Admin\LoginController@showLoginForm')->name('admin.login');
//  $this->post('admin', 'Admin\LoginController@login');
//  $this->post('logout', 'Admin\LoginController@logout')->name('logout');

//  // Registration Routes...
//  $this->get('register', 'Admin\RegisterController@showRegistrationForm')->name('register');
//  $this->post('register', 'Admin\RegisterController@register');

//  // Password Reset Routes...
//  $this->get('admin-password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
//  $this->post('admin-password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
//  $this->get('admin-password/reset/{token}', 'Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
//  $this->post('admin-password/reset', 'Admin\ResetPasswordController@reset');